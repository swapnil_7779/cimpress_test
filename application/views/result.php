<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Result set</title>
<link rel="stylesheet" id="font-awesome-style-css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" type="text/css" media="all">
<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	
	table, td, th {
    border: 1px solid black;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Result set</h1>

	<div id="body">
	<table border="1">
	<th>User id</th>	
	<th>Name </th>
	<th>Last Name</th>
	<th>email id</th>
	<tbody id = "user_data">
		<?php 
		
			foreach($result as $val) {
				?>
					<tr>
					<td><?php echo $val['user_id']; ?></td>
					<td><?php echo $val['name']; ?></td>
					<td><?php echo $val['full_name']; ?></td>
					<td><?php echo $val['url']; ?></td>
					</tr>
		<?php }
			?>
			</tbody>
			</table>
			<div align="center">
				<ul class='pagination text-center' id="pagination">
				<?php if(!empty($total_pages)){
					for($i=1; $i<=$total_pages; $i++){  
				 if($i == 1) { ?>
							<li class='active page_click'  id="<?php echo $i;?>"><a href="#"><?php echo $i;?></a></li> 
				 <?php } else {?>
				 <li class='page_click' id="<?php echo $i;?>"><a href="#"><?php echo $i;?></a></li>
				<?php }?> 
				<?php }
				}?>  
			</div>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

</body>
</html>
<script type="text/javascript">

$(document).ready(function() {
	$("body").on("click",".page_click",function(){
		
		var page = $(this).attr('id');
		 
		 $.ajax({
			 
			 'type' :"Post",
			 data : {"page":page},
			 url : "<?php echo $this->config->item('base_url'); ?>test/filter",
			 success : function(data) {
				 $(".page_click").removeClass("active");
				 $(this).addClass("active");
				 $("#user_data").html(data);
			 }
			 
		 })
		 
	})

	});


 </script>

