<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Github\HttpClient\Message\ResponseMediator;
class Test extends CI_Controller {
	
	public $limit = 10;
	public $db_name = "knp_test";
	public $table_name = "users";
	public function __construct() {
        parent::__construct();
		require_once 'vendor/autoload.php';
		$this->load->model('test_model');
		
    }
	
	public function get_page_data() {
		$data = array();
		$page=1; 
		$start_from = ($page-1) * $this->limit;
		
		$count_data = $this->test_model->get_rows_count($this->table_name);
		$total_pages = ceil($count_data / $this->limit); 
		$data['total_pages'] = $total_pages;
		$data['result'] = $this->test_model->get_data($this->table_name,$this->limit,$start_from);
		$this->load->view('result',$data);
	}
	
	public function filter() {
		$page = $this->input->post('page'); 
		$start_from = ($page-1) * $this->limit;
		$data = $this->test_model->get_data($this->table_name,$this->limit,$start_from);
		
		foreach($data as $val) {
				?>
					<tr>
					<td><?php echo $val['user_id']; ?></td>
					<td><?php echo $val['name']; ?></td>
					<td><?php echo $val['full_name']; ?></td>
					<td><?php echo $val['url']; ?></td>
					</tr>
		<?php }
		
	}
	
	public function index()
	{
		
		$client = new \Github\Client();
		$data = $client->getHttpClient()->get('/repositories');
		$repo_data = ResponseMediator::getContent($data); 
		$db_check = $this->test_model->check_db_exists($this->db_name);
		if($db_check !== "Database exists" || empty($db_check)) {
			$this->test_model->create_database($this->db_name);
		}
		$fields = array(
                        'user_id' => array(
                                                 'type' => 'INT',
												 'constraint' => 5, 
                                                 'auto_increment' => TRUE
                                          ),
                        'name' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => '255',
                                          ),
                        'full_name' => array(
                                                 'type' =>'VARCHAR',
                                                 'constraint' => '255',
                                          ),
                        'url' => array(
                                                 'type' => 'TEXT',
                                                 'null' => TRUE,
                                          ),
                );
		$this->test_model->create_new_table($this->table_name,$fields);
		
		$batch_insert = array();
		foreach($repo_data as $val) {
		
			$batch_insert[] =  array(
				"user_id" => $val['id'],
				"name" => $val['name'],
				"full_name" => $val['full_name'],
				"url" => $val['url'],
			);
		
		}
		$this->test_model->perform_batch_insert($this->table_name, $batch_insert);
		echo "inserted successfully";
		//$this->get_page_data();
	}
}

