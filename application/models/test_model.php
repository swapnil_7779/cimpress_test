<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_model {

	public function __construct() {
        parent::__construct();
		$this->load->dbforge();
		$this->load->dbutil(); 
    }

	public function create_database($db_name) {
		if ($this->dbforge->create_database($db_name))
		{
			echo 'Database created!';
			$this->load_database($db_name);
		}
	}
	
	public function check_db_exists($db_name) {
		if ($this->dbutil->database_exists($db_name))
		{
			$this->load_database($db_name);
			return "Database exists";
		}
		
	}
	public function load_database($db_name) {
		$this->load->database($db_name);
	}
	
	public function create_new_table($table_name,$fields) {
		
		/*
		* dropping the table before performing create table
		*/
		$this->dbforge->drop_table($table_name);
	
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->create_table($table_name);
	}
	
	public function perform_batch_insert($table_name,$batch_insert_data) {
	
		$this->db->insert_batch($table_name, $batch_insert_data); 

	}
	
	public function get_data($table_name,$limit,$start_from)
	{
		$this->db->limit($limit,$start_from);
		$this->db->order_by("user_id", "asc");
		$data = $this->db->get($table_name);
		return $data->result_array();
	}
	
	public function get_rows_count($table_name) {
	
		$query = $this->db->query("SELECT * FROM $table_name");
		return $query->num_rows();
	}
	
}

